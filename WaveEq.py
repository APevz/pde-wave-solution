import numpy as np
import matplotlib.pyplot as plt
import sys
import math
from scipy.integrate   import trapz

A = 0.75#amplitude
p = 7.0
Q = 1.0
BCtype = 1 #0 is Dirichlet, 1 is periodic, 2 is Neumann
velProp = 1.0 #velocity of propagation
plt.subplot(3,1,1)

for r in range(3):#r is the loop of the code, from 0 to 2
    # User Input
    # if (len(sys.argv) < 2):
    #     print "Usage: ",sys.argv[0]," <parameter filename>\n"
    #     quit()
    # inputfname = str(sys.argv[1])
    # print "Opening file: ",inputfname

    file = open("Input.txt","r")
    timeRange = float(file.readline())#Time End in seconds
    timePoints = int(file.readline())*(2**r)# #of time points
    posRange = float(file.readline())# position End in meters
    posPoints = int(file.readline())*(2**r)# of position Points, doubled with every loop
    wavelength = float(file.readline())#Wavelength
    file.close()

    dt = timeRange/timePoints
    dx = posRange/posPoints

    t = np.linspace(0,timeRange,timePoints) #mesh of points of time
    x = np.linspace(0,posRange,posPoints) #mesh of points of position calculated
    print("Grid from 0 to",posRange,"with",posPoints,"points and dx =",dx)
    print("x[0]=",x[0],"    x[posPoints-1]=",x[posPoints-1])
    #points defined as (x[i],t[n])

    C = velProp * dt / dx # Courant number - how much information(c) travels thru each grid point(dx) in each time step(dt)
    C2 = C ** 2 #help variable
    Cmax = 1
    print("C= ",C)
    if (C>Cmax):
        raise ValueError('Courant number is too large.')

    if (r == 0):
        u_h = np.zeros((posPoints,timePoints))
        Delta1 = np.zeros(timePoints)  # difference between worst and middle res
        Delta2 = np.zeros(timePoints)  # difference between middle and best res
        L2Del = np.zeros(timePoints)  # diff between L2 norms of worst res and middle res
        L2Del2 = np.zeros(timePoints)  # diff of L2 norms of middle res and best res
        Ct = np.zeros(timePoints)
        t1 = t
        x1 = x
    if (r == 1):
        u_2h = np.zeros((posPoints,timePoints))
        t2 = t
    if (r == 2):
        u_4h = np.zeros((posPoints,timePoints))
        t3 = t

    Ix = np.zeros(posPoints)#initial condition
    #arrays for storing most recent values
    u = np.zeros(posPoints, dtype=np.float_)#u^n+1_i
    u_temp = np.zeros(posPoints, dtype=np.float_)
    uTemp = np.float_
    u_1 = np.zeros(posPoints, dtype=np.float_)#u^n_i
    u_2 = np.zeros(posPoints, dtype=np.float_)#u^n-1
    Et = np.zeros(timePoints)#energy for each time
    FD = np.zeros(timePoints)#fractional deviation
    slledens = np.zeros(posPoints)
    def gaussian(x, mu, sig):#mu is center, sig is spread
        return A * np.exp((-1 * (x - mu) * (x - mu)) / (2 * sig * sig))
    Ix = gaussian(x,1.5,wavelength/7)#sigma is wavelength/7 for width to be correct

    for i in range(posPoints):
        u_1[i] = Ix[i]
        if (r == 0):
            u_h[i][0] = Ix[i]
        if (r == 1):
            u_2h[i][0] = Ix[i]
        if (r == 2):
            u_4h[i][0] = Ix[i]
    print("Test periodicity before enforcing it to see if things truly match")
    if(u_1[0] == u_1[posPoints-1]):
        print("Perfectly Periodic initial data")
    else:
        print(str(u_1[posPoints-1]) + " != " + str(u_1[0]))
    if(BCtype==0):#Dirichlet
        u_1[0] = 0
        u_1[posPoints-1] = 0
    elif(BCtype==1):#periodic
        u_1[0] = Ix[posPoints-2]
        u_1[posPoints-1] = Ix[1]
    if(r==2):
        plt.plot(x, Ix, label = 't = ' + str(t[0]))
    if(u_1[0] == u_1[posPoints-1]):
        print("Perfectly Periodic initial data")
    else:
        print(str(u_1[posPoints-1]) + " != " + str(u_1[0]))

    for i in range(1,posPoints-1):#special formula for first step
        #go left:
        #u[i] = u_1[i] + 0.5 * C * (u_1[i+1] - u_1[i-1])
        #go right:
        u[i] = u_1[i] - 0.5 * C * (u_1[i+1] - u_1[i-1])
        # split
        #u[i] = u_1[i] + 0.5 * C2 * (u_1[i+1] - 2.0 * u_1[i] + u_1[i-1])
        if (r == 0):
            u_h[i][1] = u[i]
        if (r == 1):
            u_2h[i][1] = u[i]
        if (r == 2):
            u_4h[i][1] = u[i]
        slledens[i]   = 0.5*((u[i] - u_1[i]) / (dt))**2 + 0.5*((u_1[i+1] - u_1[i-1]) / (2.0*dx)) ** 2 - Q*u[i]**(p+1.0)/(p+1.0)
        #slledens[i]   = 0.5*((u[i] - u_1[i]) / dt)**2 + 0.5*((u_1[i+1] - u_1[i-1]) / (2.0*dx)) ** 2 - u[i]**4/4.0
    if (BCtype == 0):  # Dirichlet
        u[0] = 0
        u[posPoints - 1] = 0
    elif (BCtype == 1):  # periodic
        u[0] = u[posPoints - 2]
        u[posPoints-1] = u[1]
    elif (BCtype == 2): # Neumann
        u[0] = u_1[0] - 0.5 * C * (u_1[1] - u_1[posPoints-1]) #Neumann
        u[posPoints - 1] = u_1[posPoints-1] - 0.5 * C * (u_1[0] - u_1[posPoints - 2])  #Neumann
    if (r == 0):
        u_h[0][1] = u[0]
    if (r == 1):
        u_2h[0][1] = u[0]
    if (r == 2):
        u_4h[0][1] = u[0]
    Et[0] = trapz(slledens,dx=dx)


    #now u is t = 1
    for n in range(1, timePoints):#n is the time
        energy = np.float_(0.0)
        energydensit = np.float_(0.0)
        energydensix = np.float_(0.0)
        energydensity = np.float_(0.0)
        intu = np.float_(0.0)
        #print("Running n:", n)
        u_temp[:], u_1[:], u[:] = u_1, u, u_temp  #time step
        # update all inner mesh points at time t[n+1]
        for i in range(1, posPoints-1): #equation to solve for u^n+1_i = -u^n-1_i + 2 * u^n_i + C2 * (u^n_i+1 - 2 * u^n_i + u^n_i-1)
            #u[i] = - u[i] + 2.0*u_1[i] + C2*(u_1[i+1] - 2.0*u_1[i] + u_1[i-1]) + dt**2*u_1[i]**3 #original scheme + nonlinear term
            u[i] = - u[i] + 2.0*u_1[i] + C2*(u_1[i+1] - 2.0*u_1[i] + u_1[i-1]) + Q*dt**2*u_1[i]**p #original scheme + nonlinear term

            if(r == 0):
                u_h[i][n] = u[i]
            if(r == 1):
                u_2h[i][n] = u[i]
            if(r == 2):
                u_4h[i][n] = u[i]
            slledens[i]   = 0.5*((u[i] - u_1[i]) / dt)**2 + 0.5*((u_1[i+1] - u_1[i-1]) / (2.0*dx)) ** 2 - Q*u[i]**(p+1.0)/(p+1.0)
            #slledens[i]   = 0.5*((u[i] - u_1[i]) / dt)**2 + 0.5*((u_1[i+1] - u_1[i-1]) / (2.0*dx)) ** 2 - u[i]**4/4.0
        if (BCtype == 0):  # Dirichlet
            u[0] = 0
            u[posPoints - 1] = 0
        elif (BCtype == 1):  # periodic
            u[0] = u[posPoints - 4]
            u[1] = u[posPoints - 3]
            u[posPoints - 2] = u[2]
            u[posPoints - 1] = u[3]
        elif (BCtype == 2):  # Neumann
            u[0] = -u[0] + 2.0*u_1[0] + C2*(u_1[1] - 2.0*u_1[0] + u_1[posPoints-1]) + Q*dt**2*u_1[0]**p # Neumann
            u[posPoints - 1] = -u[posPoints-1] + 2.0*u_1[posPoints-1] + C2*(u_1[0] - 2.0*u_1[posPoints-1] +
                    u_1[posPoints-2]) + Q*dt**2*u_1[posPoints-1]**p #Neumann
        Et[n] = trapz(slledens,dx=dx)
        FD[n] = (Et[n] - Et[0]) / Et[0]
        if (r == 0):
            u_h[0][n] = u[0]
            u_h[posPoints-1][n] = u[posPoints-1]
        if (r == 1):
            u_2h[0][n] = u[0]
            u_2h[posPoints-1][n] = u[posPoints-1]
            FD2 = FD
        if (r == 2):
            u_4h[0][n] = u[0]
            u_4h[posPoints-1][n] = u[posPoints-1]


        if (r == 2):
            if (n%256==0):
                plt.plot(x, u, label='t = ' + str(t[n]))
        if (r == 5):
        # height vs pos plot
            if (n == (timePoints / 4)):
                plt.plot(x, u, label='t = ' + str(t[n]))
            if (n == (timePoints / 2)):
                plt.plot(x, u, label='t = ' + str(t[n]))
            if (n == (3 * timePoints / 4)):
                plt.plot(x, u, label='t = ' + str(t[n]))
            if (n == (timePoints - 1)):
                plt.plot(x, u, label='t = ' + str(t[n]))
                plt.xlabel("Position")
                plt.ylabel("Height")
                #plt.legend()
    if (r==0):
        FD1 = FD
        Et1 = Et
    if (r==1):
        FD2 = FD
        Et2 = Et
    if (r==2):
        FD3 = FD
        Et3 = Et
#Energy vs. Time plot
# plt.subplot(3,1,2)
# plt.plot(t, Et1, label='dx = ' + str(dx) + ',dt = ' + str(dt))  # energy of the whole wave vs. time
# plt.plot(t, Et2, label='dx = ' + str(dx) + ',dt = ' + str(dt))  # energy of the whole wave vs. time
# plt.plot(t, Et3, label='dx = ' + str(dx) + ',dt = ' + str(dt))  # energy of the whole wave vs. time
# plt.xlabel("Time")
# plt.ylabel("Energy")
# plt.legend()
#Fractional Deviation plot
plt.subplot(3,1,2)
plt.plot(t1, FD1, label= 'Low res')  # frac dev of the wave vs time
plt.plot(t2, FD2, label= 'Med res')  # frac dev of the wave vs time
plt.plot(t3, FD3, label= 'High res')  # frac dev of the wave vs time
plt.xlabel("Time")
plt.ylabel("Frac. Energy Change")
#plt.legend()

for n in range(len(u_h[0])):
    for i in range(len(u_h)):
        Delta1[n] += u_2h[2*i][2*n] - u_h[i][n]
        Delta2[n] += u_4h[4*i][4*n] - u_2h[2*i][2*n]
        L2Del[n] += (Delta1[n])**2
        L2Del2[n] += (Delta2[n])**2
Delta1 /= len(u_h[0])#average of all differences at that time
Delta2 /= len(u_h[0])
for n in range(len(u_h[0])):
    L2Del[n] = math.sqrt(abs(L2Del[n]))
    L2Del2[n] = math.sqrt(abs(L2Del2[n]))
    Ct = np.log2(abs(Delta1/Delta2))
#plt.legend()

# plt.subplot(3,1,2)
# plt.xlabel("Position")
# plt.ylabel("Delta u")
# plt.plot(t1, Delta1, label = 'u_2h - u_h')
# plt.plot(t1, Delta2, label = 'u_4h - u_2h')
# plt.legend()

plt.subplot(3,1,3)
plt.xlabel("Time")
plt.ylabel("Convergence Order")
plt.ylim(0,2.0)
#plt.plot(t1, L2Del, label = 'L2norm Low Res')
#plt.plot(t1, L2Del2, label = 'L2norm High Res')
plt.plot(t1, Ct, label = 'Convergence Order')
#plt.legend()
#plt.show()
plt.savefig("convOrder.pdf")
